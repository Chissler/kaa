package com.nykredit.kundeservice.kaa;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JApplet;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.nykredit.kundeservice.swing.DateSelector;


public class Main extends JApplet
  implements ActionListener
{
  private static final long serialVersionUID = 1L;
  private CTIRConnection data;
  private DateSelector dateSelector = new DateSelector();
  private DataTable dataTable = new DataTable();
  public static String program = "KAA 18-20";

  public static void main(String[] args)
  {
    new Main();
  }

  public Main()
  {
    this.data = new CTIRConnection(program);
    setContentPane(getMainPane());
    setSize(400, 500);
    setMinimumSize(getPreferredSize());
    this.dataTable.FillTable(this.data, this.dateSelector.getPeriode(), this.dateSelector.getStart());
    setVisible(true);
  }

  private DateSelector getDateSelector() {
	this.dateSelector.setMinimumSize(new Dimension(200, 60));
    this.dateSelector.setPreferredSize(new Dimension(200, 60));
    return this.dateSelector;
  }

  private JPanel getMainPane()
  {
    JPanel p = new JPanel(new GridBagLayout());
    GridBagConstraints c = new GridBagConstraints();
    p.setOpaque(false);
    c.insets = new Insets(5, 5, 0, 0);
    p.add(getDateSelector(), c);
    c.fill = 3;
    p.add(getDataButton(), c);
    c.fill = 2;
    c.weightx = 1.0D;
    c.anchor = 18;
    p.add(new JLabel("<html><P ALIGN=CENTER><br>OBS l�rdage & s�ndage m�les der i tidsrummet 10-15.", 2), c);
    c.fill = 1;
    c.gridwidth = 5;
    c.gridy = 1;
    c.gridx = 0;
    c.weightx = 1.0D;
    c.weighty = 1.0D;
    c.insets = new Insets(5, 5, 0, 5);
    p.add(getDataTable(), c);

    c.fill = 0;
    c.weightx = 0.0D;
    c.weighty = 0.0D;

    c.anchor = 14;
    c.gridy = 2;
    c.gridx = 0;
    c.insets = new Insets(5, 5, 5, 5);
    p.add(getLabelCopyright(), c);
    return p;
  }

  private JScrollPane getDataTable() {
    JScrollPane p = new JScrollPane();
    p.setViewportView(this.dataTable);
    p.setMinimumSize(p.getPreferredSize());
    return p;
  }

  private JButton getDataButton() {
    JButton b = new JButton("Hent");
    b.addMouseListener(new MouseAdapter() {
      public void mouseClicked(MouseEvent e) {
        Main.this.dataTable.FillTable(Main.this.data, Main.this.dateSelector.getPeriode(), Main.this.dateSelector.getStart());
      }
    });
    return b;
  }

  private JLabel getLabelCopyright() {
    JLabel labelCopyright = new JLabel("Drift og Performance � Nykredit");
    labelCopyright.setFont(new Font("Dialog", 0, 10));
    return labelCopyright;
  }

  public void actionPerformed(ActionEvent arg0)
  {
  }
}
