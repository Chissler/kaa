package com.nykredit.kundeservice.kaa;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CTIRConnection extends com.nykredit.kundeservice.data.CTIRConnection
{
  public CTIRConnection(String program)
  {
    try
    {
      Connect(); } catch (SQLException e) {
      e.printStackTrace();
    }SetupSpy(program);
  }

  public ArrayList<Data> getDataArray(String dayName, String date) {
    ArrayList<Data> al = new ArrayList<Data>();
    int startTime = 18;
    int endTime = 20;
    if (dayName.contentEquals("l�rdag")) {
      startTime = 10;
      endTime = 15;
    }
    if (dayName.contentEquals("s�ndag")) {
      startTime = 10;
      endTime = 15;
    }
    try
    {
      SS_spy();

      ResultSet rs = Hent_tabel(
        "   SELECT DISTINCT AAA.INITIALER, AAA.TEAM,BBB.KORR AS KORRIGERET, BBB.IND AS INBOUND_CALLS, BBB.UDD AS OUTBOUND_CALLS, CCC.AKT AS ACTIVITIES, DDD.WEB AS WEBDESK FROM KS_DRIFT.V_TEAM_DATO AAA LEFT JOIN ( SELECT AA.INITIALER, SUM(AA.KALD_IND) AS IND, SUM(AA.KALD_UD) AS UDD, SUM(AA.KORR) AS KORR FROM KS_DRIFT.PERO_AGENT_CBR_KVART AA                                                               \t       WHERE AA.KORR != 0                                                                                         AND   AA.DATO-" + 
        startTime + "/24 >= '" + date + "'                                											" + 
        "       AND   AA.DATO-" + endTime + "/24 <= '" + date + "'                                					" + 
        "       GROUP BY AA.INITIALER ) BBB                                                                         " + 
        "   ON AAA.INITIALER = BBB.INITIALER                                                                        " + 
        "   LEFT JOIN (                                                                                             " + 
        "       SELECT AA.\"Ansvarlig Initialer\", SUM(AA.\"Antal aktiviteter\") AS AKT                             " + 
        "       FROM KS_DRIFT.DIAN_AKT_AFS_KS AA                                                                    " + 
        "       WHERE AA.\"Aktivitetstype\" NOT IN ('Indg�ende kald', 'kundepleje')                                 " + 
        "       AND   AA.\"Faktisk afslutning\"-" + startTime + "/24 >= '" + date + "'              				" + 
        "       AND   AA.\"Faktisk afslutning\"-" + endTime + "/24 <= '" + date + "'               					" + 
        "       GROUP BY AA.\"Ansvarlig Initialer\" ) CCC                                                           " + 
        "   ON AAA.INITIALER = CCC.\"Ansvarlig Initialer\"                                                          " + 
        "   LEFT JOIN (																								" +                                                                         
        "		SELECT BB.INITIALS, COUNT(*) AS WEB 																" +
        "		FROM KS_DRIFT.V_WEBDESK_SERVICECENTER BB INNER JOIN KS_DRIFT.V_TEAM_DATO							" +
        "		ON BB.INITIALS=INITIALER AND TRUNC(BB.DATETIME)=DATO												" +                                                                                  
        "		WHERE BB.DATETIME-" + startTime + "/24 >= '" + date + "' AND BB.INITIALS is not null             					" +			
        "       AND   BB.DATETIME-" + endTime + "/24 <= '" + date + "'  									             			" +		 
        "       GROUP BY BB.INITIALS ) DDD 																			" +
        "		ON AAA.INITIALER = DDD.INITIALS																		" +
        "   WHERE AAA.DATO = '" + date + "'                                                   						" + 
        "   AND   LENGTH(AAA.INITIALER) < 5                                                                         " + 
        "   AND   NOT ((BBB.IND IS NULL OR BBB.IND = 0) AND (BBB.UDD IS NULL OR BBB.UDD = 0) AND CCC.AKT IS NULL)   " + 
        "   ORDER BY AAA.TEAM, AAA.INITIALER                                                                        ");

      while (rs.next())
        al.add(
          new Data(rs.getString("INITIALER"), 
          rs.getString("TEAM"), 
          rs.getDouble("KORRIGERET"), 
          rs.getDouble("INBOUND_CALLS"), 
          rs.getDouble("OUTBOUND_CALLS"), 
          rs.getDouble("ACTIVITIES"),
      	  rs.getDouble("WEBDESK")));

    }
    catch (SQLException e)
    {
      e.printStackTrace();      
    }return al;
  }
}
