package com.nykredit.kundeservice.kaa;

import java.util.ArrayList;


import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;

import com.nykredit.kundeservice.swing.NComparator;
import com.nykredit.kundeservice.swing.NTable;
import com.nykredit.kundeservice.util.Date;
import com.nykredit.kundeservice.util.Formatter;



public class DataTable extends NTable
{
	private static final long serialVersionUID = 1L;
	private Formatter f = new Formatter();
	private Date d = new Date();
  
  public void FillTable(CTIRConnection conn, String dayName, String date) {
    DefaultTableModel tableModel = new DefaultTableModel();
    setModel(tableModel);
    ArrayList<Data> dl = conn.getDataArray(dayName, date);
    tableModel.addColumn("Initialer");
    tableModel.addColumn("Team");
    tableModel.addColumn("Korrigeret");
    tableModel.addColumn("Ind kald");
    tableModel.addColumn("Ud kald");
    tableModel.addColumn("Aktiviteter");
    tableModel.addColumn("Webdesk");

	TableRowSorter<DefaultTableModel> sorter = new TableRowSorter<DefaultTableModel>(tableModel);
    setRowSorter(sorter);
    for (int i = 2; i < getColumnModel().getColumnCount(); i++) {
      sorter.setComparator(i, new NComparator());
    }

    for (Data d : dl)
        tableModel.addRow(new Object[] { 
          d.getAgent(), 
          d.getTeam(), 
          this.d.toTTMM(d.getKorrigeret() / 60.0D / 60.0D / 24.0D, false), 
          this.f.KiloDotFill(d.getInboundCalls(), false), 
          this.f.KiloDotFill(d.getOutboundCalls(), false), 
          this.f.KiloDotFill(d.getActivities(), false),
    	  this.f.KiloDotFill(d.getWebdesk(), false) });
 }

  public boolean isCellEditable(int rowIndex, int colIndex)
  {
    return false;
  }
}
