package com.nykredit.kundeservice.kaa;

public class Data
{
  private String agent;
  private String team;
  private double inboundCalls;
  private double korrigeret;
  private double outboundCalls;
  private double activities;
  private double webdesk;

public Data(String agent, String team, double korrigeret, double inboundCalls,
		double outboundCalls, double activities, double webdesk) {
	    this.agent = agent;
	    this.team = team;
	    this.korrigeret = korrigeret;
	    this.inboundCalls = inboundCalls;
	    this.outboundCalls = outboundCalls;
	    this.activities = activities;
	    this.webdesk = webdesk;
}

public String getAgent() {
    return this.agent; } 
  public void setAgent(String agent) { this.agent = agent; } 
  public String getTeam() {
    return this.team; } 
  public void setTeam(String team) { this.team = team; } 
  public double getKorrigeret() {
    return this.korrigeret; } 
  public void setKorrigeret(double korrigeret) { this.korrigeret = korrigeret; } 
  public double getInboundCalls() {
    return this.inboundCalls; } 
  public void setInboundCalls(double inboundCalls) { this.inboundCalls = inboundCalls; } 
  public double getOutboundCalls() {
    return this.outboundCalls; } 
  public void setOutboundCalls(double outboundCalls) { this.outboundCalls = outboundCalls; } 
  public double getActivities() {
    return this.activities; } 
  public void setActivities(double activities) { this.activities = activities; }
  public double getWebdesk() {
	    return this.webdesk; } 
	  public void setWebdesk(double webdesk) { this.webdesk = webdesk;
	  
  }
}